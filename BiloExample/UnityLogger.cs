﻿using System;
using UnityEngine;

namespace Bilo_Example
{
	public class UnityLogger : Bilo.BiloLogger
	{
		public UnityLogger ()
		{
		}

		public string d (string message)
		{
			Debug.Log(message);
			return null;
		}

		public string e (string message)
		{
			Debug.LogError(message);
			return null;
		}

		public void clear ()
		{
		}
	}
}

