﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bilo;
using Bilo_Example;

public class BiloExample : MonoBehaviour, OnScanListener, OnConnectionListener, OnBlocksListener
{

	public Dropdown dropdownDevices;
	public Text textState;
	public Button buttonScan;
	public Button buttonConnect;
	public Button buttonDisconnect;
	public Button buttonBlack;
	public Button buttonRed;
	public Button buttonGreen;
	public Button buttonBlue;
	public Text textBlocks;
	public RectTransform scrollViewLog;
	public Toggle toggleLog;
	public RectTransform scrollContent;
	public Button buttonClearLog;
	public Button buttonExit;
	public Slider sliderTimer;
	public Text textTimer;
	public Text textWriteCount;

	private ListLogger logger;
	//private UnityLogger unityLogger;

	private Manager manager;

	void Start ()
	{
		dropdownDevices.ClearOptions ();
		dropdownDevices.captionText.text = "";
		textState.text = ConnectionState.Disconnected.ToString ();

		scrollViewLog.GetComponent<ScrollRect> ().onValueChanged.AddListener (valueChanged);

		this.logger = new ListLogger (scrollContent.gameObject, scrollViewLog.GetComponent<ScrollRect>(), scrollContent.GetComponentInChildren<Text> ());
        //this.unityLogger = new UnityLogger ();
        #if UNITY_ANDROID
        this.manager = new AndroidManager(logger);
        fillDropDown(((AndroidManager)manager).getDevices());
        #else
        this.manager = new BleManager (logger);
        #endif
		this.manager.setOnScanListener (this);
		this.manager.setOnConnectionListener (this);
		this.manager.setOnBlocksListener (this);

		buttonScan.onClick.AddListener (scan);
		buttonConnect.onClick.AddListener (connect);
		buttonDisconnect.onClick.AddListener (disConnect);
		buttonBlack.onClick.AddListener (setBlack);
		buttonRed.onClick.AddListener (setRed);
		buttonGreen.onClick.AddListener (setGreen);
		buttonBlue.onClick.AddListener (setBlue);
		buttonClearLog.onClick.AddListener (clearLog);
		buttonExit.onClick.AddListener (exitApp);

		toggleLog.isOn = PlayerPrefs.GetInt ("toggleLog", 0) == 1;
		scrollViewLog.gameObject.SetActive (toggleLog.isOn);
		textBlocks.gameObject.SetActive (!toggleLog.isOn);
		toggleLog.onValueChanged.AddListener (logToggle);


		sliderTimer.value = PlayerPrefs.GetFloat ("timeout", 0);
		timeoutChanged (sliderTimer.value);
		sliderTimer.onValueChanged.AddListener (timeoutChanged);

	}
	
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			manager.onDestroy ();
			Application.Quit ();
		}
		else
			manager.Update ();
		//textWriteCount.text = "" + bleManager.writeCount;
	}
	public void onConnectionChanged (ConnectionState state)
	{
		textState.text = state.ToString ();
	}
	public void onBlocksChanged (List<Block> blocks)
	{
		showBlocks (blocks);
	}
	public void onScanFinished (List<string> devices)
	{
        fillDropDown(devices);
	}
    private void fillDropDown(List<string> devices)
    {
        dropdownDevices.ClearOptions();
        foreach (string item in devices)
            dropdownDevices.options.Add(new Dropdown.OptionData(item));
        if (devices.Count == 0)
        {
            dropdownDevices.captionText.text = "";
        }
        else
        {
            dropdownDevices.captionText.text = devices[0];
            dropdownDevices.value = 0;
        }
    }


    private void scan()
	{
		manager.scan(3);
	}
	private void connect()
	{
		manager.connect (dropdownDevices.captionText.text);
	}
	private void disConnect()
	{
		manager.disconnect();
	}
	private void setBlack()
	{
		setColor (Bilo.Color.Black);
	}
	private void setRed()
	{
		setColor (Bilo.Color.Red);
	}
	private void setGreen()
	{
		setColor (Bilo.Color.Green);
	}
	private void setBlue()
	{
		setColor (Bilo.Color.Blue);
	}
	private void setColor(Bilo.Color color)
	{
		if (manager.getBase() != null)
			manager.getBase().setColor(color);
		List<Block> blocks = manager.getBlocks();
		if (blocks != null)
		{
			foreach (Block bl in manager.getBlocks())
				bl.setColor(color);
			blocks = manager.getBlocks();
			showBlocks(blocks);
		}
	}
	private void showBlocks(List<Block> blocks)
	{
		textBlocks.text = "";
		foreach (Block bl in blocks)
			textBlocks.text += bl + "\n";
	}
	private void logToggle(bool isOn)
	{
		scrollViewLog.gameObject.SetActive (isOn);
		textBlocks.gameObject.SetActive (!isOn);
		if (isOn)
			PlayerPrefs.SetInt ("toggleLog", 1);
		else
			PlayerPrefs.SetInt ("toggleLog", 0);
	}
	private void clearLog()
	{
		logger.clear();
	}
	private void valueChanged(Vector2 value)
	{
		//Debug.Log ("Value:" + value * 1000);
	}
	private void exitApp()
	{
		manager.onDestroy ();
		Application.Quit ();
	}
	private void timeoutChanged(float value)
	{
		PlayerPrefs.SetFloat ("timeout", value);
		textTimer.text = ((int)value).ToString();
		//this.bleManager.setTimeout (value / 1000f);
	}

}
