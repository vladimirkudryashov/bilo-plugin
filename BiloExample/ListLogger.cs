﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Bilo_Example
{
	public class ListLogger : Bilo.BiloLogger
	{
		private GameObject content;
		private ScrollRect scrollRect;
		private Text original;
		private List<Text> items;
		private int maxSize = 500;
		private bool error;
		public ListLogger (GameObject content, ScrollRect scrollRect, Text original)
		{
			this.content = content;
			this.scrollRect = scrollRect;
			this.original = original;
			this.items = new List<Text> (maxSize);
		}
		public string e(string message)
		{
			error = true;
			addMessage (message);
			Debug.LogError(message);
			return message;
		}
		public string d(string message)
		{
			error = false;
			addMessage (message);
			Debug.Log(message);
			return message;
		}
		public void clear()
		{
			foreach (Text t in items)
			{
				t.transform.SetParent (null);
				GameObject.DestroyImmediate (t);
			}
			items.Clear ();
		}
		private void addMessage(string message)
		{
			if (items.Count > maxSize)
				clear ();
            string messageT = ""; // string.Format("{0:0.000}", Time.realtimeSinceStartup);
			messageT =  " " + messageT + " " + message;
			int maxLen = 1300 / original.fontSize;
			if (messageT.Length <= maxLen)
			{
				addLine (messageT);
			}
			else
			{
				List<string> lines = wrap (messageT, maxLen);
				foreach (string line in lines)
					addLine(line);
			}	

			scrollRect.verticalNormalizedPosition = 0.0f;
		}
		private void addLine(string line)
		{
			Text text = GameObject.Instantiate (original);
			if (error)
				text.text = "<color=red>" + line + "</color>";
			else
				text.text = line;
			text.transform.SetParent (content.transform);
			items.Add (text);
			text.transform.localScale = original.transform.localScale;
		}
		private List<string> wrap(string text, int maxLen)
		{
			List<string> list = new List<string> ();
			int pos = 0;
			string line;
			while (pos < text.Length - 1)
			{
				int len = maxLen;
				if (len > text.Length - pos - 1)
					len = text.Length - pos - 1;
				line = text.Substring (pos, len);
				list.Add (line);
				pos += maxLen;
			}
			return list;
		}
	}
}

