﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Bilo
{
	public enum BlockType {Block4x2, Block2x2, Base10x10}
    /// <summary>
    /// Clockwise rotation of the block
    /// </summary>
	public enum Rotation {Deg0, Deg90, Deg180, Deg270}
	public enum Color {Black, Red, Green, Yellow, Blue, Magenta, Cyan, White}

    /// <summary>
    /// This class represents formatted data of the Bilo Block.
    /// </summary>
	public class Block
	{

		private BlockType type;
		private int x, y, z;
		private Rotation rotation;
		private Color[] leds;

		private byte[] blockData;
		private ColorSetter colorSetter;
        private bool clockwise = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bilo.Block"/> class.
        /// </summary>
        /// <param name="blockData">Rough block data received from device.</param>
        /// <param name="blockStorage">Block Storage to send color data.</param>
        public Block(byte[] blockData, ColorSetter blockStorage)
		{
			this.blockData = blockData;
			this.colorSetter = blockStorage;

			this.x = blockData[0];
			this.y = blockData[1];
			this.z = blockData[2];

			if ((blockData[3] & 0x04) == 0)
				this.type = BlockType.Block4x2;
			else
				this.type = BlockType.Block2x2;

			int rot = blockData[3] & 0x3;
			if (rot == 0)
				this.rotation = Rotation.Deg0;
			if (rot == 1)
				this.rotation = Rotation.Deg90;
			if (rot == 2)
				this.rotation = Rotation.Deg180;
			if (rot == 3)
				this.rotation = Rotation.Deg270;

			initLeds();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bilo.Block"/> class.
		/// </summary>
		public Block(ColorSetter colorSetter)
		{
            this.colorSetter = colorSetter;
			this.blockData = new byte[0];

			this.x = 0;
			this.y = 0;
			this.z = 0;

			this.type = BlockType.Base10x10;

			this.rotation = Rotation.Deg0;

			initLeds();
		}
		private void initLeds()
		{
			switch (type)
			{
			case BlockType.Block2x2:
				leds = new Color[2];
				break;
			case BlockType.Block4x2:
				leds = new Color[4];
				break;
			case BlockType.Base10x10:
				leds = new Color[4];
				break;
			}
			for (int i = 0; i < leds.Length; i++)
				leds[i] = Color.Black;
		}

        /// <summary>
        /// For compatibility with bilo-access bool clockwise should be false.
        /// </summary>
        /// <param name="clockwise"></param>
        public void setClockwise(bool clockwise)
        {
            this.clockwise = clockwise;
        }
        /// <summary>
        /// Gets the type of Block.
        /// </summary>
        /// <returns>The <see cref="Bilo.BlockType"/>.</returns>
        public BlockType getType()
		{
			return this.type;
		}
		/// <summary>
		/// Gets the rotation.
		/// </summary>
		/// <returns>The <see cref="Bilo.Rotation"/>.</returns>
		public Rotation getRotation()
		{
            if (clockwise)
                return this.rotation;
            else
            {
                Rotation counterclockwise = this.rotation;
                if (rotation == Rotation.Deg90)
                    counterclockwise = Rotation.Deg270;
                if (rotation == Rotation.Deg270)
                    counterclockwise = Rotation.Deg90;
                return counterclockwise;
            }
        }
		/// <summary>
		/// Gets the x coordinate on the Base.
		/// </summary>
		/// <returns>The x.</returns>
		public int getX()
		{
			return this.x;
		}
		/// <summary>
		/// Gets the y coordinate on the Base.
		/// </summary>
		/// <returns>The y.</returns>
		public int getY()
		{
			return this.y;
		}
		/// <summary>
		/// Gets the z coordinate on the Base.
		/// </summary>
		/// <returns>The z.</returns>
		public int getZ()
		{
			return this.z;
		}
		/// <summary>
		/// Sets the color of the all LEDs of the block.
		/// </summary>
		/// <returns><c>true</c>, if color was set, <c>false</c> otherwise.</returns>
		/// <param name="color">Color.</param>
		public bool setColor(Color color)
		{
			for (int i = 0; i < leds.Length; i++)
				leds[i] = color;
			if (colorSetter == null)
				return false;
			return colorSetter.setColor(color, blockData);
		}
		/// <summary>
		/// Sets the color of the specified LED of the block.
		/// </summary>
		/// <returns><c>true</c>, if color was set, <c>false</c> otherwise.</returns>
		/// <param name="color">Color.</param>
		/// <param name="led">Led number.</param>
		public bool setColor(Color color, int led)
		{
			this.leds[led] = color;
			if (colorSetter == null)
				return false;
			return colorSetter.setColor(color, blockData, led);
		}
		/// <summary>
		/// Sets the color of the all LEDs of the block.
		/// </summary>
		/// <param name="colorData">Rough color data.</param>
		public void setColor(byte[] colorData)
		{
			for (int i = 0; i < colorData.Length; i++)
			{
				int c = colorData[i];

				c = c & 0x38;
				c = c >> 3;
				leds[i * 2] = (Color)c;

				c = c & 0x07;
				leds[i * 2 + 1] = (Color)c;
			}
		}
		/// <summary>
		/// Gets the color of the specified LED.
		/// </summary>
		/// <returns>The color.</returns>
		/// <param name="led">Led.</param>
		public Color getColor(int led)
		{
			return this.leds[led];
		}
		/// <summary>
		/// Count of the LEDs.
		/// </summary>
		/// <returns>The count.</returns>
		public int ledsCount()
		{
			return leds.Length;
		}
		/// <summary>
		/// Gets the rough color data.
		/// </summary>
		/// <returns>The color data.</returns>
		public List<Byte> getColorData()
		{
			List<Byte> colorData = new List<Byte>();
			for (int i = 0; i < leds.Length / 2; i++)
			{
				byte b = 0;
				b = BlockStorage.setColor0(b, leds[i * 2]);
				b = BlockStorage.setColor1(b, leds[i * 2 + 1]);
				colorData.Add(b);
			}
			return colorData;
		}
		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>A string that represents the current object.</returns>
		/// <filterpriority>2</filterpriority>
		public override string ToString()
		{
			String s = "";
			s += type + " (" + x + "," + y + "," + z + ") " + rotation;
			for (int i = 0; i < leds.Length; i++)
				s += " " + leds[i];
			return s;
		}

	}
}

