﻿using System;
using UnityEngine;

namespace Bilo
{
	public class UnityAndroidLogger : Bilo.BiloLogger
	{
        AndroidJavaClass log;
		public UnityAndroidLogger ()
		{
        #if UNITY_ANDROID
            log = new AndroidJavaClass("android.util.Log");
        #endif
        }

		public string d (string message)
		{
			Debug.Log(message);
            if (log != null)
                log.CallStatic<int>("d", "Bilo", message);
            return null;
		}

		public string e (string message)
		{
			Debug.LogError(message);
            if (log != null)
                log.CallStatic<int>("e", "Bilo", message);
            return null;
		}

		public void clear ()
		{
		}
	}
}

