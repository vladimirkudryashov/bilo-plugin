﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bilo
{
	public class BluetoothLeProcessor
	{
		private static string BiloService = "65333333-a115-11e2-9e9a-0800200ca100";
		private static string BiloCharacteristic = "65333333-a115-11e2-9e9a-0800200ca102";
		//private static string BiloDescriptor = "00002902-0000-1000-8000-00805f9b34fb";

		private BiloLogger logger;
		private string deviceAddress;
		private List<string> devices;
		private BuetoothLeListener bluetoothLeListener;

		private bool initialized = false;

		private float scanTime;
		private bool scaning = false;

		string deviceName;
		//int connectAttempt = 0;
		//private bool connecting = false;
		private float connectTimeout;
		private bool connected = false;
		private float responceTimeout;
		private bool responce = false;
		private float timeout = 0.040f;

		public BluetoothLeProcessor (BuetoothLeListener buetoothLeListener, BiloLogger logger)
		{
			this.bluetoothLeListener = buetoothLeListener;
			this.logger = logger;
			this.devices = new List<string> ();
		}

		public void Update ()
		{
			if (scaning & Time.time > scanTime)
				stopScan ();
			if (connected & Time.time > connectTimeout)
			{
				connected = false;
				if (bluetoothLeListener != null)
					bluetoothLeListener.onConnectionChanged(ConnectionState.Connected);
				//Debug.Log("connected. time:" + Time.time);
				logger.d ("connected.");
			}

			if (responce & Time.realtimeSinceStartup > responceTimeout)
			{
				responce = false;
				if (bluetoothLeListener != null)
					bluetoothLeListener.onResponse(BiloCharacteristic);
				//Debug.Log("response " + BiloCharacteristic);
				//logger.d ("response");
			}

		}

		public void initialize ()
		{
			System.Action action = () =>
			{
				initialized = true;
				logger.d ("initialized.");
				if (bluetoothLeListener != null)
					bluetoothLeListener.onInitializationFinished(null);
			};
			System.Action<string> errorAction = (error) =>
			{
				logger.e(error);
				Debug.LogError(error);
			};
			BluetoothLEHardwareInterface.Initialize (true, false, action, errorAction);

		}
		public void deInitialize ()
		{
			System.Action action = () =>
			{
				initialized = false;
				logger.d ("deInitialized.");
			};
			BluetoothLEHardwareInterface.DeInitialize (action);
		}

		public void connect(string deviceName)
		{
			this.deviceName = deviceName;
			if (!initialized)
			{
				logger.d ("Connect: not Initialized.");
				return;
			}
			int pos = deviceName.IndexOf ("|");
			if (pos < 0)
				return;
			//connecting = true;
			if (bluetoothLeListener != null)
				bluetoothLeListener.onConnectionChanged(ConnectionState.Connecting);
			logger.d ("Connecting...");
			deviceAddress = deviceName.Substring (pos + 1);
			System.Action<string> connect = (s1) =>
			{
				//connectAttempt = 0;
				//connecting = false;
				connected = true;
				connectTimeout = Time.time + 2.0f;
			};
			/*
			System.Action<string,string> service = (s1, s2) =>
			{
				Debug.Log("service " + s1 + " " + s2);
			};
			System.Action<string,string,string> characteristic = (s1, s2, s3) =>
			{
				Debug.Log("characteristic " + s1 + " " + s2 + " " + s3);
			};
			*/
			System.Action<string> disconnect = (s1) =>
			{
				if (bluetoothLeListener != null)
					bluetoothLeListener.onConnectionChanged(ConnectionState.Disconnected);
				
				logger.d ("disconnected (connect callback) " + s1);
			};

			BluetoothLEHardwareInterface.ConnectToPeripheral (deviceAddress, connect, null, null, disconnect);
		}
		public void disConnect()
		{
			if (bluetoothLeListener != null)
				bluetoothLeListener.onConnectionChanged(ConnectionState.Disconnecting);
			logger.d ("Disconnecting...");
			System.Action<string> disconnect = (s1) =>
			{
				logger.d ("disconnected (disconnect callback) " + s1);
			};
			BluetoothLEHardwareInterface.DisconnectPeripheral (deviceAddress, disconnect);
		}
		public void scan(float duration)
		{
			if (scaning)
				stopScan ();
			logger.d ("Scan started");
			devices.Clear ();
			//string[] IDs = { BiloService };
			System.Action<string,string> addItem = (ID, name) =>
			{
				deviceAddress = ID;
				devices.Add(name + "|" + ID);
				logger.d ("Scan found " + name + "|" + ID);
			};
			scaning = true;
			scanTime = Time.time + duration;
			BluetoothLEHardwareInterface.ScanForPeripheralsWithServices (null, addItem);
		}
		public void scan(float duration, string deviceName)
		{
			if (scaning)
				stopScan ();
			logger.d ("Scan started.");
			devices.Clear ();
			//string[] IDs = { BiloService };
			System.Action<string,string> addItem = (ID, name) =>
			{
				deviceAddress = ID;
				string devName = name + "|" + ID;
				logger.d ("Scan found " + name + "|" + ID);
				if (devName == deviceName)
				{
					devices.Add(devName);
					stopScan();
				}
			};
			scaning = true;
			scanTime = Time.time + duration;
			BluetoothLEHardwareInterface.ScanForPeripheralsWithServices (null, addItem);
		}
		public void stopScan()
		{
			logger.d ("Stop Scan");
			scaning = false;
			BluetoothLEHardwareInterface.StopScan ();
			if (bluetoothLeListener != null)
				bluetoothLeListener.onScanFinished(devices);
		}
		public void subscribeQQ()
		{
			System.Action<string> notificationAction = (s1) =>
			{
				logger.d ("subscribed to " + s1);
				bluetoothLeListener.onSubscribe(s1);
			};
			System.Action<string, byte[]> action = (charID, data) =>
			{
				string s = "";
				foreach (byte b in data)
					s += b.ToString ("X") + " ";
				//Debug.Log("from " + charID + " data:" + s);
				logger.d ("Received: " + s);
				bluetoothLeListener.onDataReceived(charID, data);
			};

			BluetoothLEHardwareInterface.SubscribeCharacteristic (deviceAddress, BiloService, BiloCharacteristic, notificationAction, action);
		}
		public void subscribeWithAddress()
		{
			System.Action<string, string> notificationAction = (address, s1) =>
			{
				logger.d ("subscribed to " + s1);
				bluetoothLeListener.onSubscribe(s1);
			};
			System.Action<string, string, byte[]> action = (address, charID, data) =>
			{
				string s = "";
				foreach (byte b in data)
					s += b.ToString ("X") + " ";
				//Debug.Log("from " + charID + " data:" + s);
				logger.d ("Received: " + s);
				bluetoothLeListener.onDataReceived(charID, data);
			};

			BluetoothLEHardwareInterface.SubscribeCharacteristicWithDeviceAddress (deviceAddress, BiloService, BiloCharacteristic, notificationAction, action);
		}
		public void write(byte[] data)
		{
			System.Action<string> responseAction = (s1) =>
			{
				//logger.d ("Write callback");
				//Debug.Log("response " + s1);
				#if UNITY_ANDROID
				//responce = true;
				//responceTimeout = Time.time + timeout;
				#endif
				//bluetoothLeListener.onResponse(s1);
			};
			string s = "";
			foreach (byte b in data)
				s += b.ToString ("X") + " ";
			bool withResponce = false;
			#if UNITY_IPHONE
			//responce = true;
			//responceTimeout = Time.realtimeSinceStartup + timeout;
			#endif
			responce = true;
			responceTimeout = Time.realtimeSinceStartup + timeout;
			logger.d ("Write: " + s);
			BluetoothLEHardwareInterface.WriteCharacteristic (deviceAddress, BiloService, BiloCharacteristic, data, data.Length, withResponce, responseAction);
		}

		public void setTimeout(float timeout)
		{
			this.timeout = timeout;
		}
	}
}

