﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Bilo
{
	public class StringLogger : BiloLogger
	{
		private Text log;
		private ScrollRect scrollRect;
		private int maxSize = 5000;
		public StringLogger (Text log, ScrollRect scrollRect)
		{
			this.log = log;
			this.scrollRect = scrollRect;
		}
		public string e(string message)
		{
			addMessage (message);
			Debug.LogError(message);
			return log.text;
		}
		public string d(string message)
		{
			addMessage (message);
			return log.text;
		}
		public void clear()
		{
			log.text = "";
		}
		private void addMessage(string message)
		{
			log.text += message + "\n";
			if (log.text.Length > maxSize)
				log.text = log.text.Substring (maxSize / 2);
			scrollRect.verticalNormalizedPosition = 0.0f;
		}
	}
}

