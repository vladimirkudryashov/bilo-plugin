﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bilo
{
    /// <summary>
    /// AnroidManager class provides access to the Bilo BT Classic and LE devices in Android.
    /// </summary>
    public class AndroidManager : Manager
    {
        private BiloLogger logger;
        private AndroidJavaClass androidLogger;
        private AndroidJavaObject bilo;

        private OnBlocksListener onBlocksListener;
        private OnConnectionListener onConnectionListener;
        private bool connectionChanged = false;
        private ConnectionState connectionState = ConnectionState.Disconnected;

        private Block baseBlock;
        private List<Block> blocksTmp;
        private List<Block> blocks;
        private bool blocksChanged = false;
        private float blocksTime;
        private float blocksDelay = 0.0f;
        private bool clockwise = true;

        public AndroidManager(BiloLogger logger)
        {
            this.logger = logger;
            this.blocks = new List<Block>();

            androidLogger = new AndroidJavaClass("android.util.Log");
            
            AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            logger.d("player " + player);

            AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
            logger.d("activity " + activity);

            bilo = new AndroidJavaObject("bilo.api.bl1.Bilo", activity);
            logger.d("bilo " + bilo);

            ConnectionCallback connectionCallback = new ConnectionCallback();
            logger.d("connectionCallback " + connectionCallback);
            connectionCallback.logger = androidLogger;
            connectionCallback.manager = this;
            bilo.Call("setOnConnectionListener", connectionCallback);
            logger.d("setOnConnectionListener");


            BlocksCallback blocksCallback = new BlocksCallback();
            logger.d("blocksCallback " + blocksCallback);
            blocksCallback.logger = androidLogger;
            blocksCallback.manager = this;
            bilo.Call("setOnBlocksListener", blocksCallback);
            logger.d("setOnBlocksListener");
        }

        public List<string> getDevices()
        {
            List<string> list = new List<string>();
            string[] devices = bilo.Call<string[]>("getDevicesArr");
            logger.d("devices " + devices);
            for (int i = 0; i < devices.Length; i++)
            {
                logger.d("device " + devices[i]);
                list.Add(devices[i]);
            }
            return list;
        }

        public void connect(string deviceName)
        {
            bilo.Call("connect", deviceName);
        }

        public void disconnect()
        {
            bilo.Call("disconnect");
        }

        public Block getBase()
        {
            /*
            AndroidJavaObject baseAJO = bilo.Call<AndroidJavaObject>("getBase");
            AndroidColorSetter colorSetter = new AndroidColorSetter(baseAJO);
            byte[] blockData = baseAJO.Call<byte[]>("getBlockdata");
            Block blockNew = new Block(blockData, colorSetter);
            */
            return this.baseBlock;
        }

        public List<Block> getBlocks()
        {
            return blocks;
        }

        /// <summary>
        /// For compatibility with bilo-access bool clockwise should be false.
        /// </summary>
        /// <param name="clockwise"></param>
        public void setClockwise(bool clockwise)
        {
            this.clockwise = clockwise;
        }
        public void setBlocksDelay(float blocksDelay)
        {
            this.blocksDelay = blocksDelay;
        }

        public void onBlocksChanged(List<Block> blocks)
        {
            this.blocksTmp = blocks;
            foreach (Block block in this.blocksTmp)
                block.setClockwise(this.clockwise);
            blocksTime = Time.time;
            blocksChanged = true;
        }

        public void onConnectionChanged(ConnectionState state)
        {
            if (state == ConnectionState.Connected)
            {
                AndroidJavaObject androidBase = bilo.Call<AndroidJavaObject>("getBase");
                logger.d("androidBase " + androidBase);
                AndroidColorSetter colorSetter = new AndroidColorSetter(androidBase);
                logger.d("colorSetter " + colorSetter);
                this.baseBlock = new Block(colorSetter);
                logger.d("baseBlock " + baseBlock);

            }
            connectionState = state;
            connectionChanged = true;
        }

        public void onDestroy()
        {
            bilo.Call("onDestroy");
        }

        public void onParserEnd(Parser.Result result, string message)
        {
        }

        public void onScanFinished(List<string> devices)
        {
        }

        public void setOnBlocksListener(OnBlocksListener onBlocksListener)
        {
            this.onBlocksListener = onBlocksListener;
        }

        public void setOnConnectionListener(OnConnectionListener onConnectionListener)
        {
            this.onConnectionListener = onConnectionListener;
        }

        public void setOnScanListener(OnScanListener onScanListener)
        {
        }

        public void Update()
        {
            if (blocksChanged)
            {
                if (Time.time >= blocksTime + blocksDelay)
                {
                    blocksChanged = false;
                    blocks = blocksTmp;
                    if (onBlocksListener != null)
                        onBlocksListener.onBlocksChanged(blocks);
                }
            }
            if (connectionChanged)
            {
                connectionChanged = false;
                if (onConnectionListener != null)
                    onConnectionListener.onConnectionChanged(connectionState);
            }
        }

        public void scan(float duration)
        {
        }

        private class ConnectionCallback : AndroidJavaProxy
        {
            public AndroidJavaClass logger;
            public Manager manager;
            public ConnectionCallback() : base("bilo.api.bl1.internal.OnConnectionListener")
            {
            }
            public void onConnectionChanged(AndroidJavaObject state)
            {
                int ordinal = state.Call<int>("ordinal");
                string name = state.Call<string>("name");
                logger.CallStatic<int>("d", "Bilo", "onConnectionChanged " + ordinal + " " + name);
                ConnectionState s = (ConnectionState)ordinal;
                //if (s == ConnectionState.Connected || s == ConnectionState.Disconnected)
                    if (manager != null)
                        manager.onConnectionChanged(s);
            }
        }
        private class BlocksCallback : AndroidJavaProxy
        {
            public AndroidJavaClass logger;
            public Manager manager;
            public BlocksCallback() : base("bilo.api.bl1.internal.OnBlocksListener")
            {
            }
            public void onBlocksChanged(AndroidJavaObject blocks)
            {
                int size = blocks.Call<int>("size");
                List<Block> blocksNew = new List<Block>(size);
                AndroidJavaClass blockAJC = new AndroidJavaClass("bilo.api.bl1.Block");
				logger.CallStatic<int>("d", "Bilo", "onBlocksChanged " + size);
                for (int i = 0; i < size; i++)
                {
                    AndroidJavaObject block = blocks.Call<AndroidJavaObject>("get", i);
                    AndroidColorSetter colorSetter = new AndroidColorSetter(block);
                    colorSetter.logger = logger;
                    byte[] blockData = block.Call<byte[]>("getBlockdata");
                    Block blockNew = new Block(blockData, colorSetter);
                    logger.CallStatic<int>("d", "Bilo", "blockNew " + blockNew);
                    blocksNew.Add(blockNew);
                }
                manager.onBlocksChanged(blocksNew);
            }
        }

        private class AndroidColorSetter : ColorSetter
        {
            private AndroidJavaObject block;
            public AndroidJavaClass logger;
            public AndroidColorSetter(AndroidJavaObject block)
            {
                this.block = block;
            }
            public bool setColor(Color color, byte[] key)
            {
                int i = (int)color;
                bool ok = block.Call<bool>("setColor", i);
                return ok;
            }
            public bool setColor(Color color, byte[] key, int led)
            {
                int i = (int)color;
                return block.Call<bool>("setColor", i, led);
            }
        }
    }
}