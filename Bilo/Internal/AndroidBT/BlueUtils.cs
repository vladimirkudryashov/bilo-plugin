﻿using UnityEngine;
using System.Collections;

namespace Bilo
{
    public class BlueUtils
    {
        public static void RequestBluetooth()
        {
            AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent", "android.bluetooth.adapter.action.REQUEST_ENABLE");
            AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
            object[] args = { intent, 0 };
            activity.Call("startActivityForResult", args);

        }
        public static bool isEnabled()
        {
            AndroidJavaClass bluetoothAdapter = new AndroidJavaClass("android.bluetooth.BluetoothAdapter");
            AndroidJavaObject defaultAdapter = bluetoothAdapter.CallStatic<AndroidJavaObject>("getDefaultAdapter");
            if (defaultAdapter == null)
                return false;
            bool enabled = defaultAdapter.Call<bool>("isEnabled");
            return enabled;
        }
    }
}
