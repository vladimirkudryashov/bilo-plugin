﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Bilo
{
	public class Parser
	{
		public static byte bb;

		public static byte START_MESSAGE = (byte) 0x80;
		public static byte START_COMPASS = (byte) 0x82;
		public static byte END_MESSAGE = (byte) 0x81;
		public enum Result {OK, SyntxError, TimeError, UART_Error}

		private enum State {WaitStart, WaitData, PositionData, CompassData, WaitEnd, Error}

		private State state;

		private byte[] compassData = { 1, 2, 3, 4, 5, 6 };
		private int compassDataIndex;
		private List<Byte> positionData;
		private List<Byte> positionDataBackup = new List<Byte>();

		//private static long TIMEOUT = 15000;
		//private Timer timerOut;
		private bool parserFinished = false;
		private Result result = Result.OK;

		private OnParserEndListener onParserEndListener;


		public Parser()
		{
			reset();
		}

		public bool isFinished()
		{
			return parserFinished;
		}
		public Result getResult()
		{
			return result;
		}
		public void reset()
		{
			positionData = new List<Byte>();
			compassDataIndex = -1;
			state = State.WaitStart;
			parserFinished = true;
			//if (timerOut != null)
			//	timerOut.cancel();
			//timerOut = null;
		}
		public void startTimer()
		{
			/*
			if (timerOut != null)
				timerOut.cancel();
			timerOut = new Timer();
			timerOut.schedule(new TimerTask()
			{
				@Override
				public void run()
				{
					timeError();
				}
			}, TIMEOUT);
			*/
		}
		public List<Byte> getBlocksData()
		{
			return this.positionDataBackup;
		}
		public void receive(byte symbol)
		{
			parserFinished = false;

			switch (state)
			{
			case State.WaitStart:
				if (symbol == START_MESSAGE)
				{
					state = State.WaitData;
				}
				else
				{
					String message = getUART_error(symbol);
					if (message  != null)
						UART_error(message);
					else
						syntaxError(symbol);
				}
				break;
			case State.WaitData:
				if (symbol == START_COMPASS)
					state = State.CompassData;
				else
				{
					state = State.PositionData;
					addPositionData(symbol);
				}
				break;
			case State.PositionData:
				if (symbol == START_COMPASS)
					state = State.CompassData;
				else
					addPositionData(symbol);
				break;
			case State.CompassData:
				if (compassDataIndex < 5)
				{
					compassDataIndex++;
					compassData[compassDataIndex] = symbol;
				}
				if (compassDataIndex >= 5)
					state = State.WaitEnd;
				break;
			case State.WaitEnd:
				if (symbol == END_MESSAGE)
					end();
				else
					syntaxError(symbol);
				break;
			case State.Error:
				break;
			}
		}

		public void setOnParserEndListener(OnParserEndListener onParserEndListener)
		{
			this.onParserEndListener = onParserEndListener;
		}

		private void addPositionData(byte symbol)
		{
			positionData.Add(symbol);
		}
		private void end()
		{
			//if (timerOut != null)
			//	timerOut.cancel();
			//timerOut = null;

			//logger.debug("parser end");
			//logger.debug("parser compas: " + Worker.toStringHex(compassData, 6));

			lock(positionData)
			{
				positionDataBackup = positionData;
				positionData = new List<Byte>();
			}
			//logger.debug("parser position: " + Worker.toStringHex(positionDataBackup));
			reset();
			result = Result.OK;
			if (onParserEndListener != null)
				onParserEndListener.onParserEnd(Result.OK, "OK");
		}
		private void syntaxError(byte symbol)
		{
			String message = "syntax error: state=" + state + " symol=" + symbol;
			//Debug.Log.e(message);
			reset();
			result = Result.SyntxError;
			if (onParserEndListener != null)
				onParserEndListener.onParserEnd(Result.SyntxError, message);
		}
		private void timeError()
		{
			//if (timerOut != null)
			//	timerOut.cancel();
			//timerOut = null;
			String message = "parser: time error";
			//logger.error(message);
			reset();
			result = Result.TimeError;
			if (onParserEndListener != null)
				onParserEndListener.onParserEnd(Result.TimeError, message);
		}
		private void UART_error(String mess)
		{
			reset();
			String message = "UART error: " + mess;
			//logger.error(message);
			result = Result.UART_Error;
			if (onParserEndListener != null)
				onParserEndListener.onParserEnd(Result.UART_Error, message);
		}
		private String getUART_error(byte symbol)
		{
			if (symbol < 0x42 | symbol > 0x48)
				return null;
			String s = null;
			if (symbol == 0x42)
				s = "Receiving bang data while communication from app is occurring";
			if (symbol == 0x43)
				s = "App is trying to set more LEDs than there are blocks";
			if (symbol == 0x44)
				s = "Byte from App isn't recognized";
			if (symbol == 0x45)
				s = "UART buffer overflow, firmware can't keep up";
			if (symbol == 0x46)
				s = "Receiving bang data while communication from app should be occurring";
			if (symbol == 0x47)
				s = "Byte from App isn't recognized";
			if (symbol == 0x48)
				s = "too many blocks";

			return s;
		}
	}
}

