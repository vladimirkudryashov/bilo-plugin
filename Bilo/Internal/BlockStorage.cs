﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Bilo
{
    public interface ColorSetter
    {
        bool setColor(Color color, byte[] key);
        bool setColor(Color color, byte[] key, int led);
    }

	public class BlockStorage : ColorSetter
	{
		private RoughBlock[] rBlocks;
		private OnBlocksListener onBlocksListener;

		public BlockStorage()
		{
			this.rBlocks = new RoughBlock[0];
		}

		public void setPositionData(byte[] positionData)
		{
			if (!isDataNew(positionData))
				return;
			int n = positionData.Length / 4;
			RoughBlock[] newBlocks = new RoughBlock[n];
			for (int i = 0; i < n; i++)
			{
				newBlocks[i] = new RoughBlock(positionData, i * 4);
				RoughBlock oldBlock = findBlock(newBlocks[i].position);
				if (oldBlock != null)
					newBlocks[i].color = oldBlock.color;
			}
			rBlocks = newBlocks;
			if (onBlocksListener != null)
				onBlocksListener.onBlocksChanged(getBlocks());
		}
		public List<Byte> getColorData()
		{
			List<Byte> colorData = new List<Byte>();
			foreach (RoughBlock bl in rBlocks)
				foreach (byte b in bl.color)
					colorData.Add(b);
			return colorData;
		}
		public List<Block> getBlocks()
		{
			List<Block> result = new List<Block>();
			for (int i = 0; i < rBlocks.Length; i++)
			{
				Block block = new Block(rBlocks[i].position, this);
				block.setColor(rBlocks[i].color);
				result.Add(block);
			}
			return result;
		}
		public bool setColor(Color color, byte[] key)
		{
			RoughBlock rBlock = findBlock(key);
			if (rBlock == null)
				return false;
			for (int i = 0; i < rBlock.color.Length; i++)
			{
				rBlock.color[i] = setColor0(rBlock.color[i], color);
				rBlock.color[i] = setColor1(rBlock.color[i], color);
			}
			return true;
		}
		public bool setColor(Color color, byte[] key, int led)
		{
			RoughBlock rBlock = findBlock(key);
			if (rBlock == null)
				return false;
			int i = led / 2;
			if (i > rBlock.color.Length)
				return false;
			int led01 = led - i * 2;
			if (led01 == 0)
				rBlock.color[i] = setColor0(rBlock.color[i], color);
			else
				rBlock.color[i] = setColor1(rBlock.color[i], color);
			return true;
		}

		private RoughBlock findBlock(byte[] key)
		{
			for (int i = 0; i < rBlocks.Length; i++)
			{
				bool found = true;
				for (int j = 0; j < 4; j++)
				{
					if (rBlocks[i].position[j] != key[j])
					{
						found = false;
						break;
					}
				}
				if (found)
					return rBlocks[i];
			}
			return null;
		}
		private bool isDataNew(byte[] positionData)
		{
			int n = positionData.Length / 4;
			if (n != rBlocks.Length)
				return true;
			for (int i = 0; i < rBlocks.Length; i++)
				for (int j = 0; j < 4; j++)
					if (rBlocks[i].position[j] != positionData[i * 4 + j])
						return true;

			return false;
		}
		public static byte setColor0(byte trg, Color color)
		{
			int c = (int)color;
			c = c << 3;
			int result = trg;
			result = result & 0xC7;
			result = result | c;
			return (byte) result;
		}
		public static byte setColor1(byte trg, Color color)
		{
			int c = (int)color;
			int result = trg;
			result = result & 0xF8;
			result = result | c;
			return (byte) result;
		}
		public void setOnBlocksListener(OnBlocksListener onBlocksListener)
		{
			this.onBlocksListener = onBlocksListener;
		}

		private class RoughBlock
		{
			public byte[] position;
			public byte[] color;

			public RoughBlock(byte[] positionData, int offset)
			{
				this.position = new byte[4];
				for (int i = 0; i < 4; i++)
					position[i] = positionData[i + offset];

				int len = 1; // 2x2 block
				if ((position[3] & 0x04) == 0)
					len = 2; // 2x4 block
				this.color = new byte[len];
				for (int i = 0; i < color.Length; i++)
					color[i] = 0;
			}
		}


	}
}

