﻿using System;
using System.Collections.Generic;

namespace Bilo
{
	public enum ConnectionState 
	{
		Disconnected,
		Connecting,
		Connected,
		Disconnecting
	}

	public interface OnScanListener
	{
		void onScanFinished (List<string> devices);
	}
	public interface OnConnectionListener
	{
		void onConnectionChanged (ConnectionState state);
	}

	public interface BuetoothLeListener
	{
		void onInitializationFinished (string result);
		void onConnectionChanged (ConnectionState state);
		void onScanFinished (List<string> devices);
		void onRetrieveFinished (List<string> devices);
		void onSubscribe (string characteristic);
		void onUnSubscribe (string characteristic);
		void onResponse (string characteristic);
		void onDataReceived (string characteristic, byte[] data);
	}

	public interface OnBlocksListener
	{
		void onBlocksChanged (List<Block> blocks);
	}

	public interface OnParserEndListener
	{
		void onParserEnd (Parser.Result result, String message);
	}

	public interface Manager : OnScanListener, OnBlocksListener, OnConnectionListener, OnParserEndListener
	{
        /// <summary>
        /// This method should be called from the Update method of the <b>one</b> of the scripts.
        /// </summary>
        void Update();

        /// <summary>
        /// This method should be called on Application.Quit().
        /// </summary>
        void onDestroy ();

        /// <summary>
        /// Scan for BLE devices. Result will be returned in callback 
        /// you set by setOnScanListener(OnScanListener onScanListener) method.
        /// If Scan finds device you previously connected, Connect method will be called for this device
        /// </summary>
        /// <param name="duration">Scan duration.</param>
        void scan(float duration);

        /// <summary>
        /// Attempts to connect to the specified device. onConnectionChanged callback will be called after this method start and after connecting.
        /// </summary>
        /// <param name="deviceName">Device name and device ID separated by "|".</param>
        void connect (string deviceName);

        /// <summary>
        /// Disconnect currently connected device. onConnectionChanged callback will be called after  disconnecting.
        /// </summary>
        void disconnect ();

        /// <summary>
        /// Gets the blocks.
        /// </summary>
        /// <returns>The blocks.</returns>
		List<Block> getBlocks ();

        /// <summary>
        /// Gets the base block.
        /// </summary>
        /// <returns>The base block.</returns>
        Block getBase ();

        /// <summary>
        /// For compatibility with bilo-access clockwise should be false.
        /// </summary>
        /// <param name="clockwise"></param>
        void setClockwise(bool clockwise);

        /// <summary>
        /// Allows to hide Contact bounce (also called chatter)
        /// </summary>
        /// <param name="blocksDelay"></param>
        void setBlocksDelay(float blocksDelay);

        /// <summary>
        /// Sets the listener on scan completion.
        /// </summary>
        /// <param name="onScanListener">On scan finish listener.</param>
        void setOnScanListener (OnScanListener onScanListener);

        /// <summary>
        /// Sets the listener on change of blocks list.
        /// </summary>
        /// <param name="onBlocksListener">On blocks listener.</param>
        void setOnBlocksListener (OnBlocksListener onBlocksListener);

        /// <summary>
        /// Sets the listener on <c>ConnectionState</c>.
        /// </summary>
        /// <param name="onConnectionListener">On connection listener.</param>
        void setOnConnectionListener (OnConnectionListener onConnectionListener);
	}

	public interface BiloLogger
	{
		string d (string message);
		string e (string message);
		void clear ();
	}
}