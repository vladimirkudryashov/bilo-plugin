﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Bilo
{
	/// <summary>
	/// BleManager class provides access to the Bilo BLE device in Android and iOS.
	/// </summary>
	public class BleManager : Manager
	{
		private BiloLogger logger;
		private Parser parser;
		private BlockStorage blockStorage;
		private BluetoothLeProcessor processor;

		private Block baseBlock;
		private List<Byte> data = new List<Byte> ();
		private ConnectionState connectionState = ConnectionState.Disconnected;

        private List<Block> blocksTmp;
        private List<Block> blocks;
        private bool blocksChanged = false;
        private float blocksTime;
        private float blocksDelay = 0.0f;
        private bool clockwise = true;

        private string deviceName;
		private List<string> devices;

		private OnScanListener onScanListener;
		private OnConnectionListener onConnectionListener;
		private OnBlocksListener onBlocksListener;

		private float receiveTimeout = 2.0f;
		private float receiveTime = Time.realtimeSinceStartup + 5;
		private byte[] baseData = {0x80, 0x0, 0x0, 0x81 };

		public int writeCount = 0;

		/// <summary>
		/// Initializes a new instance of the <see cref="Bilo.BleManager"/> class.
		/// </summary>
		/// <param name="logger">Logger. Can't be null.</param>
		public BleManager (BiloLogger logger)
		{
			this.logger = logger;
			this.parser = new Parser ();
			this.parser.setOnParserEndListener (this);
			this.blockStorage = new BlockStorage ();
			this.blockStorage.setOnBlocksListener (this);
			this.processor = new BluetoothLeProcessor (new BleListener (this), logger);
			this.baseBlock = new Block (null);
			this.baseBlock.setColor (Color.Green, 3);
			this.connectionState = ConnectionState.Disconnected;
            this.blocks = new List<Block>();
            this.blocksTmp = new List<Block>();

            processor.initialize ();

            #if UNITY_ANDROID
            //this.biloAndroidBT = new BiloAndroidBT(logger, this);
            #endif
        }

		/// <summary>
		/// This method should be called from the Update method of the <b>one</b> of the scripts.
		/// </summary>
		public void Update ()
		{
			processor.Update ();
			if (connectionState == ConnectionState.Connected &&
			    Time.realtimeSinceStartup > receiveTime + receiveTimeout)
			{
				logger.e ("response timeout elapsed");
				receiveTime = Time.realtimeSinceStartup + 2;
				//processor.deInitialize ();
				//processor.initialize ();
				processor.write(baseData);
			}
            if (blocksChanged)
            {
                if (Time.time >= blocksTime + blocksDelay)
                {
                    blocksChanged = false;
                    blocks = blocksTmp;
                    if (onBlocksListener != null)
                        onBlocksListener.onBlocksChanged(blocks);
                }
            }
        }

        /// <summary>
        /// Internal callback
        /// </summary>
        /// <param name="devices">Devices.</param>
        public void onScanFinished (List<string> devices)
		{
            /*
            if (biloAndroidBT != null)
                foreach (string device in biloAndroidBT.getDevices())
                    devices.Add(device);
            */
			this.devices = devices;
			if (onScanListener != null)
				onScanListener.onScanFinished (devices);
			if (devices.Contains (deviceName))
				connect (deviceName);
		}

		/// <summary>
		/// Internal callback
		/// </summary>
		/// <param name="blocks">Blocks.</param>
		public void onBlocksChanged (List<Block> blocks)
		{
            this.blocksTmp = blocks;
            foreach (Block block in this.blocksTmp)
                block.setClockwise(this.clockwise);
            blocksTime = Time.time;
            blocksChanged = true;
		}

		/// <summary>
		/// Internal callback
		/// </summary>
		/// <param name="state">State.</param>
		public void onConnectionChanged (ConnectionState state)
		{
			if (onConnectionListener != null)
				onConnectionListener.onConnectionChanged (state);
			this.connectionState = state;
			if (state == ConnectionState.Connected)
			{
				receiveTime = Time.realtimeSinceStartup;
				PlayerPrefs.SetString ("deviceName", deviceName);
                //if (!classic)
                    processor.subscribeWithAddress();
            }
        }

		/// <summary>
		/// Internal callback
		/// </summary>
		/// <param name="result">Result.</param>
		/// <param name="message">Message.</param>
		public void onParserEnd (Parser.Result result, String message)
		{
			// TODO Auto-generated method stub

		}

		/// <summary>
		/// This method should be called on Application.Quit().
		/// </summary>
		public void onDestroy ()
		{
            //if (biloAndroidBT != null)
                //biloAndroidBT.onDestroy();
            processor.deInitialize();
        }

        /// <summary>
        /// Scan for BLE devices. Result will be returned in callback 
        /// you set by setOnScanListener(OnScanListener onScanListener) method.
        /// If Scan finds device you previously connected, Connect method will be called for this device
        /// </summary>
        /// <param name="duration">Scan duration.</param>
        public void scan (float duration)
		{
            processor.scan (duration);
		}

		/// <summary>
		/// Attempts to connect to the specified device. onConnectionChanged callback will be called after this method start and after connecting.
		/// </summary>
		/// <param name="deviceName">Device name and device ID separated by "|".</param>
		public void connect (string deviceName)
		{
			if (this.connectionState == ConnectionState.Connected)
				return;
			this.deviceName = deviceName;
			onConnectionChanged (ConnectionState.Connecting);
            /*
            if (biloAndroidBT != null)
            {
                if (biloAndroidBT.getDevices().Contains(deviceName))
                {
                    classic = true;
                    biloAndroidBT.connect(deviceName);
                    return;
                }
                else
                    classic = false;

            }
            */
            processor.connect (deviceName);
		}

		/// <summary>
		/// Disconnect currently connected device. onConnectionChanged callback will be called after  disconnecting.
		/// </summary>
		public void disconnect ()
		{
			onConnectionChanged (ConnectionState.Disconnecting);
            //if (biloAndroidBT != null)
                //biloAndroidBT.disconnect();
            processor.disConnect();
        }

        /// <summary>
        /// Gets the blocks.
        /// </summary>
        /// <returns>The blocks.</returns>
        public List<Block> getBlocks ()
		{
			return blocks;
		}

		/// <summary>
		/// Gets the base block.
		/// </summary>
		/// <returns>The base block.</returns>
		public Block getBase ()
		{
			return this.baseBlock;
		}

        /// <summary>
        /// For compatibility with bilo-access clockwise should be false.
        /// </summary>
        /// <param name="clockwise"></param>
        public void setClockwise(bool clockwise)
        {
            this.clockwise = clockwise;
        }

        /// <summary>
        /// Allows to hide Contact bounce (also called chatter)
        /// </summary>
        /// <param name="blocksDelay"></param>
        public void setBlocksDelay(float blocksDelay)
        {
            this.blocksDelay = blocksDelay;
        }

        /// <summary>
        /// Sets the listener on scan completion.
        /// </summary>
        /// <param name="onScanListener">On scan finish listener.</param>
        public void setOnScanListener (OnScanListener onScanListener)
		{
			this.onScanListener = onScanListener;
		}

		/// <summary>
		/// Sets the listener on change of blocks list.
		/// </summary>
		/// <param name="onBlocksListener">On blocks listener.</param>
		public void setOnBlocksListener (OnBlocksListener onBlocksListener)
		{
			this.onBlocksListener = onBlocksListener;
		}

		/// <summary>
		/// Sets the listener on <c>ConnectionState</c>.
		/// </summary>
		/// <param name="onConnectionListener">On connection listener.</param>
		public void setOnConnectionListener (OnConnectionListener onConnectionListener)
		{
			this.onConnectionListener = onConnectionListener;
		}

		/// <summary>
		/// Sets the timeout for write operation. Recommended value is 0.
		/// If "Characteristic write error" occurs value should be increased.
		/// </summary>
		/// <param name="timeout">Write timeout in seconds.</param>
		public void setTimeout(float timeout)
		{
			this.processor.setTimeout(timeout);
		}

        private class BleListener : BuetoothLeListener
		{
			private BleManager manager;
			private List<byte> data;
			private byte[][] split;
			private int splitIndex;

			private float deltaMin = 1000000;
			private float deltaMax = 0;
			private float writeTime;

			public BleListener (BleManager manager)
			{
				this.manager = manager;
				this.data = new List<byte> ();
			}

			public void onInitializationFinished (string result)
			{
				manager.deviceName = PlayerPrefs.GetString ("deviceName", "");
				if (manager.deviceName != "")
					manager.processor.scan (3, manager.deviceName);
			}

			public void onConnectionChanged (ConnectionState state)
			{
				manager.logger.d ("ConnectionChanged: " + state);
				if (state == ConnectionState.Disconnected && manager.connectionState == ConnectionState.Connecting)
				{
					//Debug.LogError ("<color=red>Connect failed</color>");
					manager.logger.d ("Connect failed");
					if (manager.deviceName != "")
						manager.processor.scan (3, manager.deviceName);
				}
				if (state == ConnectionState.Connected)
				{
					deltaMin = 1000000;
					deltaMax = 0;
				}
				manager.onConnectionChanged (state);
			}

			public void onScanFinished (List<string> devices)
			{
				manager.onScanFinished (devices);
			}

			public void onRetrieveFinished (List<string> devices)
			{
			}

			public void onSubscribe (string characteristic)
			{
			}

			public void onUnSubscribe (string characteristic)
			{
			}

			public void onResponse (string characteristic)
			{
				float delta = Time.realtimeSinceStartup - writeTime;
				splitIndex++;
				if (splitIndex < split.Length)
				{
					if (delta < deltaMin && manager.writeCount > 1)
					{
						deltaMin = delta;
						//manager.logger.d ("N=" + manager.writeCount + " deltaMin=" + string.Format("{0:0.000}",delta * 1000f));
					}
					if (delta > deltaMax && manager.writeCount > 1)
					{
						deltaMax = delta;
						//manager.logger.d ("N=" + manager.writeCount + " deltaMax=" + string.Format("{0:0.000}",delta * 1000f));
					}


					if (split[splitIndex].Length > 0)
					{
						//Thread.Sleep(200);
						manager.processor.write (split[splitIndex]);
					}
				}
			}

			public void onDataReceived (string characteristic, byte[] value)
			{
				manager.receiveTime = Time.realtimeSinceStartup;
				if (value == null)
					return;
				foreach (byte symbol in value)
					manager.parser.receive (symbol);
				if (manager.parser.isFinished () & manager.parser.getResult() == Parser.Result.OK)
				{
					List<byte> blocksData = manager.parser.getBlocksData ();
					byte[] positionData = new byte[blocksData.Count];
					for (int i = 0; i < positionData.Length; i++)
						positionData [i] = blocksData [i];
					manager.blockStorage.setPositionData (positionData);
					data.Clear ();
					data.Add ((byte)0x80);
					data.AddRange (manager.baseBlock.getColorData ());
					if (manager.parser.getResult () == Parser.Result.OK)
						data.AddRange (manager.blockStorage.getColorData ());
					data.Add ((byte)0x81);

					manager.writeCount++;
					manager.receiveTime = Time.realtimeSinceStartup;
					writeData (data);
					writeTime = Time.realtimeSinceStartup;
					//parser.startTimer();
				}

			}

			private void writeData (List<byte> data)
			{
				if (manager.connectionState != ConnectionState.Connected)
					return;
				int splitLen = data.Count / 20 + 1;
				int lastLen = data.Count - (splitLen - 1) * 20;
				split = new byte[splitLen][];
				for (int i = 0; i < split.Length - 1; i++)
				{
					split[i] = new byte[20];
					for (int j = 0; j < 20; j++)
						split[i][j] = data[i * 20 + j];
				}
				byte[] last = new byte[lastLen];
				split[split.Length - 1] = last;
				for (int i = 0; i < last.Length; i++)
					last[i] =  data[(splitLen - 1) * 20 + i];
				
				splitIndex = 0;
				manager.processor.write (split[splitIndex]);

				/*
				byte[] arr = new byte[data.Count];
				for (int i = 0; i < arr.Length; i++)
					arr [i] = data [i];
				manager.processor.write (arr);
				*/
			}

		}
	}
}

